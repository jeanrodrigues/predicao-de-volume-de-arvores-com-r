
install.packages("caret")
library(caret)

# Importando base de dados
volumes <- read.csv2("~/git/predicao-de-volume-de-arvores-com-r/Volumes.csv", stringsAsFactors=FALSE)

# Setando a seed do random 
set.seed(7)

#removendo a primeira coluna  com o sequencial da linha
volumes <- volumes[,-1]

indices <- createDataPartition(volumes$VOL, p=0.80, list=FALSE)

treino <- volumes[indices,]
teste <- volumes[-indices,]

summary(treino)

# Treinamdo o modelo Random Forest
rf <-  train(VOL~., data=treino, method="rf")

# Treinamdo o modelo SVM
svm <- train(VOL~., data=treino, method="svmRadial")

# Treinamdo o modelo RNA
rna <- train(VOL~., data=treino, method="nnet",linout = TRUE , trace=FALSE, na.action = na.omit )
warnings()
# Treinamdo o modelo Alometrico
alom <- nls(VOL ~ b0 + b1*DAP*DAP*HT, treino, start=list(b0=0.5, b1=0.5))

rfpredicted <- predict(rf,teste)
rnapredicted <- predict(rna,teste)
svmpredicted <- predict(svm,teste)
alompredicted <- predict(alom,teste)

help(nnet)
 
# formula R2 tradicional
1 - sum( ( rfpredicted - teste$VOL ) ^ 2 ) / sum( ( teste$VOL - mean(teste$VOL)  ) ^ 2  ) 
1 - sum( ( svmpredicted - teste$VOL ) ^ 2 ) / sum( ( teste$VOL - mean(teste$VOL)  ) ^ 2  )
1 - sum( ( alompredicted - teste$VOL ) ^ 2 ) / sum( ( teste$VOL - mean(teste$VOL)  ) ^ 2  )

corrna <- cor.test(rnapredicted ,teste$VOL)
r2rna <- R2(rnapredicted , teste$VOL)

corrf <- cor.test(rfpredicted ,teste$VOL)
r2rf <- R2(rfpredicted , teste$VOL)

corsvm <- cor.test(svmpredicted , teste$VOL)
r2svm <- R2(svmpredicted , teste$VOL)

coralom <- cor.test(alompredicted , teste$VOL)
r2alom <- R2(alompredicted , teste$VOL)


print(paste0(c("rf   corr: " , corrf$estimate, " r2: " , r2rf)))
print(paste0(c("svm  corr: " , corsvm$estimate, " r2: " , r2svm)))
print(paste0(c("rna  corr: " , corrna$estimate, " r2: " , r2rna)))
print(paste0(c("alom corr: " , coralom$estimate, " r2: " , r2alom)))

# Considerando a correlacao e o valor R2 calculado dos modelos, o modelo svm foi levemente melhor que os outros
# modelos treinados, os quais o resultado do R2 foi similar


